# Test self-signed certification using Nginx

The purpose of this project is to create an environment to test self-signed certification for different projects.

## Start/Stop microservice
To start the microservice, run the following:

  `docker-compose up -d`

To kill a microservice, run the following:

  `docker-compose down`
  
## SSL certificates
- Generate private key and certificates

    `sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout nginx-selfsigned.key -out nginx-selfsigned.crt`
    
- Decode the content of a certificate

    `openssl x509 -in nginx-dot-net.crt -text -noout`

## How to get an artifact from Nginx server using self-signed certificate

### .NET project dependencies
- Change the container-name in the `docker-compose.yml` file 

      container_name: api.nuget.org
      
- Hit the endpoint using self-signed certificate

  `curl --cacert nginx-dot-net.crt -v https://api.nuget.org/v3/index.json`
  
### Maven project dependencies
- Change the container-name in the `docker-compose.yml` file 

      container_name: repo.maven.apache.org
      
- Hit the endpoint using self-signed certificate

  `curl --cacert nginx-maven.crt  -v https://repo.maven.apache.org`
  
  `curl --cacert nginx-maven.crt  -v http://repo.maven.apache.org:8000`
  
  `curl --cacert nginx-maven.crt  -v https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-resources-plugin/2.6/maven-resources-plugin-2.6.pom`
